# first build process for building the app
FROM golang:alpine as builder
LABEL stage=intermediate

RUN apk --no-cache add git

WORKDIR /go/src/github.com/DidierStockmans/shippy-service-consignment

COPY . .

RUN go get -u github.com/golang/dep/cmd/dep
RUN dep init && dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

# new build process for running the app
FROM alpine:latest

RUN apk --no-cache add ca-certificates
RUN mkdir /app
WORKDIR /app

COPY --from=builder /go/src/github.com/DidierStockmans/shippy-service-consignment/shippy-service-consignment .

CMD ["./shippy-service-consignment"]